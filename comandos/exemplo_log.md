# Comando $log$
    git log: Lista os commits do repositório

# Exemplo
    Lennon@Notebook-Vitor MINGW64 ~/Desktop/Gerenciamento e versionamento de codigo/pratica01 (main)
    $ git log
    commit 610f6b913b4254e3a8b822c948a277b81b2bd5ea (HEAD -> main)
    Author: Vitor Medeiros <lennon_vms@hotmail.com>
    Date:   Wed Sep 20 20:24:02 2023 -0300

    doc: Documentação do comando git init

    commit 26362a94c8029f558b4005fc29e85c0f919c5c3a (origin/main)
    Author: Vitor Medeiros <lennon_vms@hotmail.com>
    Date:   Wed Sep 20 19:52:33 2023 -0300

    feat: Alteração para nome completo

    commit d5d2ccf70fcf3f0cc99f96e057330b71c9a624dc
    Author: Vitor Medeiros <lennon_vms@hotmail.com>
    Date:   Wed Sep 20 19:49:44 2023 -0300

    doc: Inserção de experiências pessoais

    commit d503eabe55ea79e175929261cd0d54739c345bc5
    Author: Vitor Medeiros <lennon_vms@hotmail.com>
    Date:   Wed Sep 20 13:48:59 2023 -0300

    doc: Inserção de dados pessoais