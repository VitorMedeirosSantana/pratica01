# Comando $init$
    git init [nome-da-pasta]: Cria a estrutura basica de um repositório GIT na pasta local ou na pasta indicada.
    A pasta .git (oculta) contém as informações do repositório GIT.
    Para verificar pastas/arquivos ocultos pelo terminal, basta acrescentar a opção -a para ver tudo.

# Exemplo
    Lennon@Notebook-Vitor MINGW64 ~/Desktop/Gerenciamento e versionamento de codigo/pratica01 (main)
    $ ls -a
    ./  ../  .git/  README.md

    Lennon@Notebook-Vitor MINGW64 ~/Desktop/Gerenciamento e versionamento de codigo/pratica01 (main)
    $ ls .git
    COMMIT_EDITMSG  config  description  FETCH_HEAD  HEAD  hooks/  index  info/  logs/  objects/  refs/