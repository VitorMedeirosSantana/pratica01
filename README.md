# Informações pessoais
    Nome: Vitor Medeiros Santana
    Matricula: 191363
    E-mail: vmedeiros@pucminas.br

# Experiência
    Trabalho no laboratório da engenharia elétrica da PUC-MG (Contagem).
    Auxílio às práticas dos professores e alunos e no desenvolvimento de projetos acadêmicos.